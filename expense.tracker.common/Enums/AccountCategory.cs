﻿using System;
using System.ComponentModel;

namespace expense.tracker.common
{
    [Flags]
    public enum AccountCategory
    {
        [Description("Cash")]
        Cash = 1,
        [Description("Account")]
        Account = 2,
        [Description("Card")]
        Card = 4,
    }
}
