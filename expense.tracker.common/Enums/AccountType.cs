﻿using System;
using System.ComponentModel;

namespace expense.tracker.common
{
    [Flags]
    public enum AccountType
    {
        [Description("Income")]
        Income = 1,
        [Description("Transfer")]
        Transfer = 0,
        [Description("Expense")]
        Expense = -1,
    }
}
