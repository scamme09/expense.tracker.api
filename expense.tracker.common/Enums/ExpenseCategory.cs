﻿using System;
using System.ComponentModel;

namespace expense.tracker.common
{
    [Flags]
    public enum ExpenseCategory
    {
        [Description("Food")]
        Food = 1,
        [Description("Transportation")]
        Transportation = 2,
        [Description("Bills")]
        Household = 3,
        [Description("Fashion")]
        Fashion = 4,
        [Description("Health")]
        Health = 5,
        [Description("Leisure")]
        Gift = 6,
        [Description("Other")]
        Other = 10
    }
}
