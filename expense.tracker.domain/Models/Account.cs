﻿using expense.tracker.common;
using System;

namespace expense.tracker.domain.Models
{
    public class Account
    {
        public AccountType Type { get; set; }
        public DateTime Date { get; set; }
        public AccountCategory AccountCategory { get; set; }
        public ExpenseCategory ExpenseCategory { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }

    }
}
